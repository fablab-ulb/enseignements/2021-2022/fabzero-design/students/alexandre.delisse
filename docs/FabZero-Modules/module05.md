# 5. Usinage assisté par ordinateur


#SHAPERTOOL

Cette semaine nous avons eu l'occasion de nous initier à une nouvelle machine, la *Shaper tool*.

L'usinage assisté se rapproche beaucoup de la découpe assistée, on part d'un fichier vectoriel et avec les bons réglages la machine s'occupe de la découpe.


# Présentation de la machine : la *Shaper*

Pour une présentation détaillée de la machine, rendez-vous [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md).

La Shaper est une défonceuse assistée par ordinateur. C'est une mini CNC portable qui permet de fraiser des matériaux sans limite de taille.

![](./images/protocole58.png)


**Caractéristiques de la machine**

- Profondeur de découpe maximale : 43mm
- Diamètre du collet : 8mm
- Format prit en charge : SVG
- Mandrin pour la fraise : 6mm

![](./images/protocole55.png)


# Utilisation de la machine

Pour mener à bien sa découpe, il faut bien fixer à l'aide de serre-joints la planche de bois sur un plateau de découpe qui va être abimer. Il ne faut donc pas découper directement sur une table. Il faut ensuite s'assurer d'avoir assez d'espace pour tourner autour de notre planche.
Une fois le matériaux bien fixé au support à l'aide d'un scotch double face ou de serre-joints il faut **délimiter l'espace de travail**.

On place du **shaper tape**, un scotch avec des motifs de domino. On le place de manière à ce qu'il y ait du scotch dans le champs de vision de la caméra du Shaper sur l'entiereté du dessin.

![](./images/protocole59.png)
![](./images/protocole52.png)

# Découpe

**préparation**

Avant de commencer la découpe, on s'assure d'avoir la bonne fraise en fonction du matériaux que l'on découpe.
Voici un petit guide

![](./images/protocole511.png)

![](./images/protocole53.png)



Une fois les préparatifs terminés, on allume la machine et on branche le cable de l'aspirateur à la Shaper.

**scan**

Une fois la machine allumée, on doit calibrer l'espace de travail. La Shaper calibre son espace grâce au scotch c'est pour cela que sa caméra doit toujours en avoir dans son champs.
En deplaçant la Shaper sur notre support, on peut voir la calibration sur l'ecran qui nous indiquera les zones qu'elle a du mal à reconnaitre. Si il y a des zones non parametré il faut rajouter du scotch shaper (si le domino est rouge).

![](./images/protocole54.png)

**réglage**


 On règle ensuite, en fonction de si l'on veut graver ou découper,
la profondeur de découpe. Il est recommandé de mettre 1mm de plus que le support si l'on souhaite une découpe, pour corriger une possible elasticité du matériaux.

C'est pour cela aussi qu'il est conseillé de ne pas couper directement sur une table mais d'avoir une deuxieme couche sous notre matériaux.

![](./images/protocole56.png)

On étalonne ensuite le point 0 en appuyant sur **Z_touch**

**découpe**

On allume l'aspirateur et la shaper, on choisi sur l'écran si on veut une découpe sur la ligne a l'interieur ou a l'exterieur ensuite on appuie sur le bouton vert, la fraise descend et on deplace la shaper en suivant le trait de découpe que l'on voit à l'ecran.


![](./images/protocole57.png)


Une fois notre forme finie, on appuie sur le bouton orange et la fraise remonte, on coupe ensuite l'aspirateur  et la shaper.






## CNC

**Présentation de la machine**

CNC veut dire Computer Numerical Control, ça signifie qu'un ordinateur converti un design en un code que l'ordinateur va utiliser pour controler un bras qui coupe et taille de la matière. C'est une machine dite soustractive.


Étant donné que les routeurs CNC utilisent un large éventail de fraises différentes pour obtenir une variété de résultats et de finitions, ils sont également fabriqués pour couper et graver un large éventail de matériaux. Voici une liste de quelques-uns des matériaux les plus courants : :

- toutes sortes de mousses
- les panneaux de MDF
- les feuilles acrylique
- les panneaux de MFC
- l'aluminium
- feuille de fibre de carbone
- du bois
- du caoutchouc ...

Contrairement à la découpe laser qui utilise des fichiers 2D, la CNC nécessite des fichiers 3D et une préparation avant découpe.



## Préparation FUSION360


Une fois votre modèle 3D fini il faut passer en mode *Fabriquer*
![](./images/cnc1.png)

Ensuite il faudra créer une configuration de découpe. Dans ce menu, il faudra encoder les trois axes (x;y;z) du modèle 3D puis rentrer les dimensions du *brut*, c'est le matériaux que le souhaite découper dans l'onglet s'y référant .


![](./images/cnc2.png)



![](./images/cnc3.png)


Une fois qu'on a configuré les axes et le brut on configure les poches ouvertes, fermées et traversantes avec l'outil *poches 2D*.

![](./images/cnc4.png)

![](./images/cnc5.png)

On configure ici l'outil on peut voir que j'ai sélectionné une fraise de 8mm de diamètres mais il faudra encorder en fonction de votre materiel. On va devoir ensuite sélectionner la géométrie des poches.

![](./images/cnc6.png)

Une fois les poches sélectionnées on va parametrer la profondeur de découpe.

![](./images/cnc7.png)

Ici je n'ai modifié que la *hauteur sur le fond* de -0.2mm pour m'assurer que la fraise va bien completement traverser le brut vu qu'ici mes poches sont traversantes.
Il faut ensuite régler les passes:

![](./images/cnc8.png)

Ici il est super important de se rappeler que la profondeur de passe ne peut pas être superieur à la moitié du diametre de la fraise. Comme on l'a vu plus haut j'ai une fraise de 8mm je décide donc que la passe sera de 4mm.

![](./images/cnc9.png)

Une fois ces réglages effectués fusion nous montre une simulation du chemin que va empreinter la fraise. Si on ne remarque pas de problème particulier avec le chemin on peut passer aux réglages des contours exterieur de la découpe avec l'outil *contours 2D*.

![](./images/cnc10.png)

![](./images/cnc11.png)

![](./images/cnc13.png)

Pareil que pour les poches on selectionne une géométrie et on règle les passes.

Petite astuce, j'ai conçu une pièce qui a la même largeur que le plateau de la CNC, pour éviter un temps de découpe inutile  sur les bords lateraux j'ai cliqué une seconde fois sur une droite d'un des contours et j'ai défini un contour ouvert.

![](./images/cnc14.png)

![](./images/cnc12.png)

Une fois tous les contours paramétrés on peut simuler la découpe et voir si il y à un problème.

![](./images/cnc15.png)

Voilà mon fichier avec un clair le brut, les axes et en foncé les pièces.

![](./images/cnc16.png)

On  peut avoir une estimation du temps de découpe

![](./images/cnc17.png)

J'ai fini d'encoder mon volume, je peux maintenant sauvergarder le code créer par fusion avec l'outil *post-traiter*

![](./images/cnc18.png)

Une fois le fichier de destination encoder, je peux passer sur la machine CNC.

## Préparation machine

Il faut ouvrir le code dans le programme *kinetic NC* sur l'ordinateur relié a la CNC. On règle le point 0 de la machine en déplaçant la rampe avec les flèches du clavier et on clique sur 0 sur les axes x et y lorsque elle est au bonne endroit.

![](./images/cnc20.png)

Des lignes de codes s'affichent en bas de l'ecran grâce à celles ci on peut faire une dernière verification des réglages notamment sur la profondeur de découpe et la fraise

![](./images/cnc21.png)

Une fois que les réglages sont ok, on paramètre le Z=0 à l'aide de l'outil suivant qu'on place sous la fraise

![](./images/z01.png)

Il faut ensuite cliquer sur

![](./images/z0.png)

Une fois le Z0 parametré ol ne reste plus qu'a visser le brut au plateau pour éviter qu'il bouge  pendant la découpe, allumer l'aspirateur de la rampe et lancer la découpe et appuyer sur start.

**Module 2 : Découverte de Fusion 360**



**Déssine moi une baignoire**


Cette semaine nous avons eu l'occasion de commencer la conception 3D avec le logiciel Fusion 360.


Pour installer Fusion 360 il vous suffit de vous rendre sur ce [site ](https://www.autodesk.com/products/fusion-360/personal) et télécharger le logiciel selon votre système d'exploitation.



Je commence tout d'abord à dessiner un rectangle en 2D avec l'outil rectangle dans "esquisse"

![](./images/protocole21.png)

![](./images/protocole22.png)

J'extrude ensuite le rectangle pour avoir un volume. J'utilise l'outil extrusion.

![](./images/protocole23.png)

![](./images/protocole24.png)

Une fois le volume crée je dois le creuser, j'utilise donc l'outil "coque".

![](./images/protocole25.png)

![](./images/protocole26.png)

Pour créer le fond galbé de la baignoire je fais une nouvelle esquisse avec l'outil ligne et je dessine la forme sur le plan vertical de la baignoire.

![](./images/protocole27.png)


J'extrude ensuite cette nouvelle forme avec l'outil extruder.

![](./images/protocole28.png)


J'utilise une derniere fois l'outil extruder pour former les bords de la baingoire.

![](./images/protocole29.png)

Je décide ensuite d'utiliser l'outil render.

![](./images/protocole211.png)

Grâce a l'outil rendu j'ajoute des materiaux en les faisants glisser sur la forme à laquelle je veux ajouter la texture.

![](./images/protocole212.png)


**paramètre**

Sur fusion il est possible d'utiliser des paramètres que l'on ajoute au dessin.

Pour cela il faut ouvrir l'onglet "modifier" et ouvrir les paramètres

![](./images/protocole213.png)

Il faut ensuite créer un parametre utilisateur et encoder les differentes valeurs que l'on souhaite parametrer et les rapporter au tableau des paramètres.

![](./images/protocole214.png)

vous pouvez retrouver mon fichier 3D ici:
[modele3D](https://a360.co/3opzAch)

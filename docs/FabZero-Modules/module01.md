**MODULE 1 : découverte de GITlab**



Aujourd'hui dans le cadre de l'option Design nous avons pu nous familiariser avec le logiciel **gitlab**.

**Installation de GitLab**

Pour commencer il faut télécharger GitLab sur ce [site]https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git).




Gitlab c'est quoi ?
Gitlab, c’est une plateforme qui permet d’héberger et de gérer des projets web de A à Z. Elle offre la possibilité de gérer ses dépôts Git et ainsi de mieux appréhender la gestion des versions de vos codes sources.

**Préparation du Git**

Avant de commencer, il faut savoir que Gitlab va de paire avec le terminal de votre ordinateur. Le terminal ? Oui oui le truc qu'on ouvre sans vraiment comprendre pourquoi lorsque l'on crack autocad etc..

Une fois le terminal ouvert, il faut taper une liste de "commandes" afin d'y encoder son nom d'utilisateur : **git config --global use.name "nomchoisi"**
ainsi que son adresse mail : **git config *--global user.email "votreadresseemail"**

Une fois votre git configuré il va falloir créer une clé de sécurité, il en existe de plusieurs types mais celle qui nous interesse le plus puisqu'elle permet une identification unique est la clé SSH.

![](./images/protocole1.png)


Une fois votre clé créee, vous pouvez la retrouver dans votre ordinateur dans un dossier intitulé .SSH.

![](./images/protocole13.png)






Il va maintenant falloir ajouter cette clé a votre gitlab. Pour cela il faut se rendre sur les paramètres de votre compte gitlab ensuite sur l'onglet Clé SSH et y ajouter votre clé que vous avez copié collé depuis le terminal.


![](./images/protocole14.png)

![](./images/protocole15.png)





Enfin, il va falloir créer un dossier réferentiel sur votre bureau, puis ouvrir le terminal depuis ce dossier et copier la clé SSH précédée de la commande "git clone" cette action permet à votre ordinateur de fusionner avec votre gitlab.

![](./images/protocole12.png)

**Installation et configuration d'Atom**

_Bien qu'on soit capable de rédiger sur Git il est bien plus pratique d'utiliser le logiciel **Atom** qui nous permet de travailler directement depuis notre ordinateur.
Pour télécharger Atom c'est par [ici](https://atom.io/)

Grâce aux étapes précédentes avec la clé SSH, vos fichiers Atom sont liés à Gitlab. Pour mettre à jour les informations entre les différentes plateformes, il faut premièrement cliquer sur File -> Save. Nos modifications arrivent alors dans la fenêtre Git d'Atom dans Unstaged Changes. Il faut alors cliquer sur Stage All. Ensuite, en cliquant sur See all staged changes, où je peux verifier ce qui a été modifié ou pas. Une fois la vérification faite, j'entre dans la fenêtre Commit message ce que j'ai modifié (afin de toujours savoir ce qui a été fait) puis je clique sur Commit to main. La dernière étape consiste à appuyer sur Fetch, d'entrer à nouveau son mot de passe puis de cliquer sur Push et le tour est joué, nos modifications ont été envoyées sur Gitlab en un clic.

![](./images/protocole16.png)

![](./images/protocole17.png)

**Configuration du site web**

Pour améliorer le visuel du site, il faut changer les données inscrites par défaut sur Gitlab. Pour cela, je me rend sur Gitlab pour les modifier. J'ouvre le fichier mkdocs.yml et je clique sur Edit. Je peux alors changer le nom du site, le thème, etc...
Pour ma part, j'ai modifié les lignes 2 à 6, ainsi que la ligne 11 j'ai choisi le visuel minty.

![](./images/protocole18.png)

**Le Markdown**

Il existe différents langage de code. Nous avons utiliser le Markdown qui utilise la nomenclature suivante :

![](./images/protocole19.png)

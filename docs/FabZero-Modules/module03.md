# 3. Impression 3D

<div style="text-align: justify">

Cette semaine nous avons eu l'occasion de tester l'impression 3D. Nous avons utilisé des imprimantes 3D PRUSA.


**installation**

Pour les utiliser vous devez d'abord installer l'application PrusaSlicer sur ce [site](https://www.prusa3d.com/drivers/).

![](./images/protocole31.png)

Ce driver sert à générer un fichier que l'imprimante pourra lire et imprimer.

**Préparation**

Pour commencer il faut exporter notre fichier depuis FUSION360 en STL.

![](./images/protocole31.png)

Une fois le fichier exporté au bon format il suffit de l'ouvrir dans PrusaSlicer.

![](./images/protocole391.png)

On peut voir que l'objet est trop grand pour le plateau, il faut donc le redimensionner, ici à 5% de sa taille.



Il faut ensuite configurer des paramètres en fonction de notre objet et de l'imprimante, ici :

![](./images/protocole35.png)

Il faut ensuite changer les réglages d'impression:

![](./images/protocole36.png)

J'ai ensuite changé les paramètres de remplissage à 20%

![](./images/protocole37.png)

Une fois les réglages terminés il faut exporter le g-code en cliquant sur "découper maintenant" en bas à droite. On peut voir maintenant les différentes couches verticales qui forment notre objet:

![](./images/protocole392.png)

Il faut ensuite exporter le G-code

![](./images/protocole39.png)

**Impression**

Avant de lancer l'impression il faut s'assurer :

- Vérifier s'il y a assez de filaments ou s'assurer d'avoir la bonne couleur.
- s'assurer que le plateau est propre.
- fermer les fenêtres, en effet si la fenêtre est ouverte l'impression risque de se décoller du plateau.

Une fois ses étapes préalables ont été effectuées on peut lancer l'impression qui débutera lorsque le plateau et la buse auront atteint les bonnes températures.

**Résulat**

![](./images/1pied1.png)




[mon fichier STL](./baignoire/pied2.stl)

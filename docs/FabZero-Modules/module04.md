# 4. Découpe assistée par ordinateur

<div style="text-align: justify">


L'exercice est simple, il faut, à partir d'un socket, d'une ampoule et d'une feuille de polypropylène de créer une lampe en utilisant la **découpe assistée par ordinateur**.


# La découpe Laser

La découpeuse laser est une machine qui lit des fichiers vectoriels et  découpe de la matière grâce à un laser concentré sur une très petite surface. On peut, grâce à une découpeuse laser avec une **intensité** et une **vitesse** variables,  **découper ou graver** à souhait de nombreux matériaux.

# Les matériaux

En plus du polyprolylène, il est possible de découper :
- du bois (3/4mm)
- de l'acrylique / plexiglass
- du papier
- du carton
- cuir et textiles

**Attention** il y a des matériaux qu'il ne faut absolument pas utiliser sous peine d'abîmer la machine ou de s'intoxiquer avec des fumées nocives :
- MDF
- ABS
- polyethylène épais
- matériaux composite fibreux
- métaux
- PVC
- cuivre
- téflon
- vinyle / similicuir
- résine phénolique


Je recommande de **toujours faire un test** sur un petit morceaux et de voir comment la découpe se déroule si l'on a un doute sur un matériaux pas encore utlisé et ne faisant pas partie de cette liste.  



# La *Lasersaur*


La machine que j'ai utilisé s'appelle la *Lasersaur*. Il s'agit d'une machine OpenSource construite par le Fablab. Moins technique que leur deuxieme machine (dotée d'une camera qui permet de suivre en temps réel la découpe) elle offre une grande surface de découpe de 120cm x 60cm.



![](./images/protocole48.png)

# Recommandation d'usage.

L'utilisation de la découpeuse laser peut sembler très facile et ludique mais une mauvaise peut rapidement s'avérer catastrophique.

- Comme dit précédemment il faut **bien choisir son matériaux**.

- Toujours ouvrir la vanne d'air comprimé.
- Allumer l'extracteur d'air et le ventilateur.
- Bien fermer le capot de la machine.
- Surveiller la machine pendant **l'entièreté de la découpe**.




# Préparation du fichier de découpe

Pour utiliser une découpeuse laser, il faut créer un fichier 2D vectoriel au format STL, j'ai utilisé le logiciel de déssin autocad.

Il est important de signaler que la découpeuse laser ne reconnait que les couleurs, il n'est pas nécessaire d'utiliser différent calques, il faut  s'assurer que chaque ligne correspondant à une découpe spécifique possède bien une couleur.
D'experience je  vous conseille de faire les gravures/pliages (chez moi en vert) en premier suivies ensuite des découpes interieures (noir) et finir par les découpes exterieures (rouge) afin d'éviter que le matériaux que l'on découpe bouge.

![](./images/protocole41.png)

Grâce aux livrets que l'on trouve au fablab on peut savoir en fonction du matériaux que l'on découpe quelle vitesse et quelle puissance correspondent le mieux au rendu que l'on veut avoir.

Ensuite en fonction des couleurs que l'on a choisi, on règle la vitesse et la puissance du laser. Plus le laser est lent et la vitesse puissante plus ca va couper.




![](./images/protocole43.png)

# Découpe

La découpe s'est très bien passée

![](./images/protocole47.png)
Vous voila avec les pièces finies. Il ne reste plus qu'à les assembler au socket.


# La lampe




![](./images/protocole44.png)
![](./images/protocole45.png)
![](./images/protocole46.png)

Vous voila maintenant en possession d'une **Light-némone**

[lampeSVG](./lampe/design_lampe_finale.svg)

[lampeDXF](./lampe/deisng_lampe_finale.dxf)

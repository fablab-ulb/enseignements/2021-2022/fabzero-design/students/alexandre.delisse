## PROJET FINAL

## Hine Table

![](./image/ferme.png)



## Déconstruire la fonction

*Table (n.f) : Meuble composé d'un plateau horizontal reposant sur un ou plusieurs pieds ou supports*

Une table c'est donc une surface plane parallèle au sol et reposant sur celui-ci.
Une table à manger quant à elle doit être suffisamment grande pour que l'on puisse y poser des couverts et une assiette, ses pieds ne doivent pas gêner une personne assise.

## Résoudre une problématique

*Un salon pour deux tables*


![](./image/finalesalon1.png)


De nombreux appartements, notamment les appartements étudiants,  ne disposent pas, faute de place, un véritable salon, une cuisine et une salle à manger. On se retrouve donc bien souvent à manger assis sur un canapé.
Or, on le sait, ce n'est pas du tout ergonomique de manger sur un canapé, on est trop bas et l'assiette est trop loin de nous.
Pour résoudre ce problème j'ai décidé de design-er une table basse qui se transforme en table à manger.

## Dimension

Je me suis fixé une contrainte, mon design doit être réalisable avec qu'une seule machine du Fablab.
Réalisant un meuble en bois, mon choix s'est porté vers la CNC pourquoi ? Parce que c'est une machine d'une extrême précision ce qui facilite grandement la réalisation d'emboitement par exemple.
Il a donc fallu intégrer la dimension maximum du plateau de la CNC
qui est de 600mm x 1000mm à la conception de la table.

Je suis allé voir dans le *Neufert* qui est un célèbre ouvrage de référence en matière d'esquisse architecturale et qui reprend entre autres pas mal de dimensions de mobiliers notamment les tables.
On peut y lire que pour une table à manger, la largeur minimum conseillée est de 55cm, et  la largeur optimale pour une table de 4/6 couverts est entre 60 et 80cm.

On peut donc voir que la largeur optimale minimum correspond à la largeur maximum du plateau de la CNC. Avec une CNC plus grande j'aurai peut-être pu avoir une table allant jusqu'à 80cm de large mais étant donné que je design une table qui répond à un manque d'espace le choix des 60cm reste pertinent.

Pour la hauteur de la table, toujours dans le *Neufert* j'ai pu voir que la hauteur parfaite pour une table à manger est de 75cm, cette hauteur permet d'avoir le plateau de la table à hauteur des coudes et d'avoir la meilleure position pour manger.

La table basse doit en revanche avoir une hauteur plus ou moins égale à la hauteur de l'assise du canapé sans toutefois dépasser 45cm. J'ai donc pris comme référence mon canapé qui est à 40cm de haut.

![](./image/finaledimension.png)

![](./image/dimension111.png)


Pour ce qui est de la longueur du plateau, j'ai conservé la longueur de ma table basse (90cm) qui me convient très bien.





## Esquisse


J'ai dessiné dans un premier temps une table de base qui va me servir à réfléchir à un plan d'attaque.

J'ai pour cela utilisé du carton et la *lasercut* j'ai utilisé différentes épaisseurs de carton pour visualiser le travail de la CNC.
Cette première réalisation m'a permis de prendre conscience à laquelle j'allais être confronté pour réaliser une table modulable en hauteur et m'a permis de pouvoir visualiser les différents angles d'attaque possible.

![](./image/opendesk.png)

![](./image/laser.png)



 **1. Travail sur le pied**

J'ai d'abord pensé à avoir un axe de rotation entre la partie supérieure et inférieure du pied.


![](./image/essaipied1.png)
![](./image/essaipied11.png)

Je suis satisfait de la table lorsqu'elle est basse mais lorsque les pieds sont déployés je trouve que le dédoublement ne rend pas bien sans compter les éléments que je vais devoir mettre en place pour stabiliser la table et l'épaisseur de l'axe de rotation.

Je décide donc de changer d'approche.

![](./image/essaipied2.png)
![](./image/essaipied21.png)

Cette solution me convient beaucoup plus. Les pieds s'emboitent entre eux et la forme générale me convient autant basse que haute. Ce dispositif me permet de reprendre les codes de la table basse avec des pieds plus épais et de la table à manger plus fins.
Il me reste à trouver l'emboitement le plus solide tout en étant restant facile à glisser...

![](./image/1pied.png)

![](./image/1pied1.png)

![](./image/1pied2.png)

Enfin c'est ce que je pensais jusqu'au pré-jury où l'on n'a pas manqué de me faire remarquer que ce système d'assemblage de pieds n'est pas du tout pratique et que le déploiement nécessite trop d'actions. C'est le problème du *designer concepteur* lorsque l'on manque de recul sur notre travail on passe à côté de choses évidentes. J'étais pourtant convaincu par mon système et je dois maintenant repartir de zéro.

 **2. Boulon Blanc : vers de nouveaux horizons**

J'étais jusqu'à présent focus sur un mouvement de pliage ou de rotation du pied pour changer la hauteur de plateau mais après les échecs précédents j'étais aussi à court d'idée. Après un tour sur le net pour voir ce que faisait *la concurrence* je suis tombé sur un studio de design français [*Boulon Blanc*](https://www.kickstarter.com/projects/712852295/boulon-blanc-the-next-generation-of-transformable?lang=fr) qui pour moi a réalisé une très bonne table transformable mêlant des techniques issues de l'industrie aeronavale et de l'horlogerie. Ils ont mit plus de deux ans à imaginer et concevoir cette table en utilisant plus de trois cents pièces usinées sur mesure et un système très élaboré de câbles. On peut qualifier cette table de "High tech high design" et il est bien entendu évident que ce n'est pas avec une CNC et en deux mois que je peux réaliser une table comme ils l'ont faite; mais j'ai pu envisager la conception du système différemment.

![](./image/boulonblanc.png)

 **3. Le déclic**

Grâce au travail de Boulon Blanc, notamment le mouvement des pieds qu'ils ont mis en place, m'est venue l'idée de la *table-charnière*

![](./image/char1.png)

![](./image/char2.png)

![](./image/char3.png)

En manipulant un peu la manipulant la maquette je me suis rendu compte que la table pouvait prendre cette position et j'ai donc fait une deuxième version avec une meilleure ligne

![](./image/char4.png)

![](./image/char6.png)

![](./image/char5.png)


 **4. confrontation maquette/prototype**

La consigne de l'atelier est claire nous n'avons pas le droit d'utiliser des clous ni vis et autres quincailleries j'ai donc dû imaginer un système de charnières en bois à grande échelle que j'ai testé en mousse

![](./image/vid11.mp4)

Le résultat est convaincant mais de retour au fablab je me rends compte qu'il me sera impossible de réaliser un élément important de la conception,le percement parfaitement droit sur toute la largeur de la table pour y insérer un axe de rotation entre les deux parties. Si j'avais eu en ma possession une foreuse à colonne de plus de 60cm de haut j'aurai sans aucun doute opté pour cette variation. Sur conseil des fab-manageusesj'abandonne le système de rotation et choisi un système d'encastrement certes un peu plus long à utiliser mais réalisable à 100% avec les outils du Fablab.

Je réalise un premier test en bois avec la ShaperTool à l'échelle 1/4.

![](./image/bois1.png)

![](./image/bois01.png)

![](./image/bois02.png)

![](./image/bois3.png)

![](./image/bois4.png)

![](./image/bois5.png)

![](./image/bois2.png)

**A ce stade il me reste deux problèmes à résoudre :**

- le maintien de la position ouverte (à manger)

- l'encastrement des pieds qui sont pour l'instant simplement collés aux plateaux latéraux.

**4. Modélisation 3D**

Je modélise l'encastrement des pieds dans les plateaux latéraux

![](./image/piedcote0.png)

![](./image/piedcote.png)

Et pour le maintien de la table j'opte pour des tasseaux en bois qui viennent s'encastrer dans les pieds ce qui va permettre  de solidifier  l'assemblage des trois pièces.  

![](./image/piedcote2.png)

Je me suis ensuite inspiré des tables de ping-pong pour rendre pratique l'utilisation de ces tasseaux en ayant juste à tirer ou à pousser une barre en bois.


![](./image/piedcote3.png)

Je passe donc sur fusion afin de modéliser les pièces et préparer les fichiers pour la CNC.


![](./image/365.png)

![](./image/361.png)

![](./image/360.png)

![](./image/363.png)

![](./image/362.png)

![](./image/364.png)

![](./image/3635.png)


Pour réaliser ces découpes j'ai besoin quatres panneaux de 60x80 et deux de 100x60  *Brico* propose deux tailles de panneaux 125x61 et 122x250.

![](./image/panneaux.png)

Voici comment les planches doivent être découpées avant d'être usinées.


**Modificiations de dernière minute**

Je crains que les emboitements puissent ne pas se faire correctement j'ajoute donc en dernier recours des *dog bone*, géométries arrondies qui va faciliter les emboitements et encastrements. J'ai pu très facilement les réaliser grâce au plug in fusion [*NiftyBDogBone*](https://apps.autodesk.com/FUSION/en/Detail/Index?id=3534533763590670806&appLang=en&os=Win64&autostart=true)
Je les ai placés aux endroits voulus

![](./image/dogbone.png)

**Problème machine**

  Je suis de nouveau face à un problème, impossible d'utiliser la largeur totale de la CNC, je suis obligé de prévoir un dégagement de 4mm de chaque côté... Je suis bon pour refaire une 3D avec une largeur de 59.2cm au lieu de 60. Et recommencer les découpes déjà faites. Le saut d'échelle m'est pour l'instant fatal et soulève pas mal de problème, je ne peux que regretter de ne pas avoir travaillé le 1/1 plus tôt. Franchement à deux jours du jury je me serai bien passé de ça.

## Assemblage

Surprise, le martyr (plaque qui sert de support au brut sur la CNC) est vraiment très abimé et n'est plus droit, la première découpe n'est pas traversante

![](./image/tb22.png)


![](./image/tb21.png)

Je finis donc la découpe avec un marteau et un burin mais ...

Encore un problème, sans raison apparente et malgré la supervision de Christophe qui m'a fait la formation, la CNC n'a pas fraisé exactement comme indiqué et les emboitements que j'ai prévus ne passe pas. J'ai perdu 1mm de vide et j'ai 1mm de plein en trop.


![](./image/tb0.png)

Je tente une dernière modification sur fusion 360 et je relance un test, prions pour que ça fonctionne sinon j'ai bien peur de n'avoir qu'une table basse.

![](./image/tb.png)





**Dans le *money time* et après trois jours d'essais l'emboîtement est au top**



![](./image/tucoco0.png)

![](./image/tucoco.png)

Dernière journée avant le jury, je passe à brico acheter le bois qu'il me manque pour finir et finir le montage.

![](./image/bricco.png)




## MONTAGE

Avant de monter les pièces entre elle, il faut coller ensemble **pied_fin** et **pied_fin_2** // **pied_large** et **pied_large_2** // et les tasseaux deux par deux.

Ensuite on encastre ***côtés*** dans ***plateau***. Et on place ensuite délicatement ***plateau*** dans ***cotés*** avec au choix le placements des tasseaux au préalable

![](./image/montage1.png)

## RESULTAT


![](./image/debout.png)

![](./image/ferme.png)

J'étais super content du résultat jusqu'à ce que je test la solidité.

Gros gros problème de dernière minute et qui sera malheureusement présent pour la remise la table manque cruellement de contreventement. Je n'ai pas le temps d'apporter des modifications et me retrouve dos au mur.

Je test en catastrophe des solutions possibles en vue d'une modofication posterieur au jury :


![](./image/soluss2.png)

Notamment par un système de cablage (Seul solution réalisable chez moi en dernière minute) mais des câbles placés ainsi va gêner la peronne assise.

![](./image/soluss1.png)

Je test une nouvelle fois avec des câbles posés différements. Le résultat est probant je vais donc dessiner un nouveau pied en bois.

![](./image/finale.png)

![](./image/finale2.Png)

Avec une journée ou deux de plus voici la table que j'aurai aimé vous présenter.


## CONCLUSION


Malgré un résultat final qui a un goût d'inachevé, (temporaire car je sais que je vais apporter les modifications nécessaires à son bon fonctionnement même si le jury est passé) je reste tout de même ravi d'avoir choisi l'option DESIGN, pendant les études d'architecture on s'est habitué à travailler sur des projets fictifs papiers ce qui peut nous pousser parfois à décaler une courbe de niveau, réajuster quelque chose comme ça nous arrange, ici rien de tout ça, tout est concret, le contact avec la matière, les machines, le résultat, il n'y a pas de triche et j'en parle en connaissance de cause, à 12h du jury je me rends compte que ma table n'est pas suffisamment contreventée et j'ai cru comprendre que je n'étais pas le seul à avoir été confronté durement à la réalité de l'échelle (et de son poids) 1:1.
En tout cas grâce à l'atelier j'ai eu la chance de pouvoir concevoir et fabriquer une table, la mienne, intégralement, avec des machines que je ne maitrisais absolument pas il y a 3 mois (voire même une semaine pour certaines) et j'ai adoré l'expérience. C'est avec plaisir que je me replongerai dans des projets perso ou non au fablab
et la conception assistée par ordinateur est quelque chose qui a changé ma façon de voir les choses, plusieurs fois pendant la semaine intense de construction je me suis retrouvé a me demander comment tel ou tel objet avait été ou pouvait être construit

































## Fournitures

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1 | Panneaux multiplex grande taille   | 59.99 €| https://www.brico.be/fr/bouwmaterialen/hout/multiplex-panelen/sencys-vochtwerend-multiplex-paneel-'topplex'-250x122x1-8cm/5356349   |
|  1  | Panneaux multiplex petite taille    |  18.49 €| https://www.brico.be/fr/construction/bois/panneaux-multiplex/panneau-multiplex-sencys-'topplex'-125x61x1-2cm/5356353  |        |
| 1   | Colle à bois  |  5.00 €| https://www.brico.be/fr/construction/mat%C3%A9riaux-de-construction/adh%C3%A9sifs-colles/colles-de-montage-et-accessoires/colle-%C3%A0-bois-pattex-pu-construct-60gr/5272347   |        |


## Nom et logo

J'ai choisi d'appeler ma table la *Hine table* en référence à la charnière en anglais *hinge* et par soucis de prononciation et de sonorité j'ai retiré le G, le logo lui aussi rappelle de manière subtile une charnière.

![](./image/logo.png)





## Dessins techniques et modèle 3D.

**3D**

[Pied Fin](https://a360.co/3KI2DlJ)

[pied fin2](https://a360.co/3nZGxRW)

[côtés](https://a360.co/3G3CfiL)

[pied large2](https://a360.co/3KGkIAJ)

[pied large](https://a360.co/3rKYG6U)

[plateau](https://a360.co/3Ay2pJ6)

**2D**

[table DXF](./DXF/table_DXF_592.dxf)

**Fichier CNC**

[cotés](./CNC_final/côtés.nc/)

[pied_fin](./CNC_final/pied_fin.nc)

[pied_fin2](./CNC_final/pied_fin_2.nc)

[pied_large](./CNC_final/pied_large.nc)

[pied_large2](./CNC_final/pied_large_2_592.nc)

[plateau](./CNC_final/plateau.nc)

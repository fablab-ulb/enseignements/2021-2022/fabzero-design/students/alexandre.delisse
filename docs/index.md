## A propos de moi

Bonjour à tous, je m'appelle Alexandre Delisse et  voilà  mon site sur lequel vous allez pouvoir suivre pas à pas les différentes étapes de mon processus créatif durant le semestre passé avec le cours de DESIGN.


![](image/protocole0.jpg)


**Mon parcours**

Je suis né à Marseille où j'ai vécu avant de venir à Bruxelles pour devenir architecte.


**Travaux précédents**

Au cours des ateliers que j'ai eu l'occasion de suivre, j'ai pu travailler sur différentes choses. De la conception de vêtements au cours d'un workshop jusqu'à repenser tout un système politique avec l'atelier APA, voici quelques illustrations.


![](./image/protocole01.png)

![](./image/protocole02.png)

![](./image/protocole03.png)







**Objet**


Dans le cadre du studio de design j'ai décidé de travailler une table. L'enjeu étant que cette table puisse passer de table basse à table à manger.

![](./image/scene.png)
